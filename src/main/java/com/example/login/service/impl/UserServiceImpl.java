package com.example.login.service.impl;

import com.example.login.dto.UserDto;
import com.example.login.entity.User;
import com.example.login.repository.UserRepo;
import com.example.login.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final UserService userService;

    @Override
    public List<User> allUser() {
        return userRepo.findAll();
    }

    @Override
    public User registr(User user) {
        return userRepo.save(user);
    }

    @Override
    public String getId(Long id) {
        if (userRepo != null) {
            userRepo.findById(id);
        }
        return "Not found id" + id;
    }

    @Override
    public void deleteUserId(Long id) {
        userRepo.deleteById(id);
    }

//    @Override
//    public User login(LoginRequestDTO loginRequest) {
//        return ;
//    }

    @Override
    public Long update(Long id, User user) {
        if (userRepo.findById(id).isPresent()) {
            User user1 = userRepo.findById(id).get();
            user1.setName(user.getName());
            user1.setSurname(user.getSurname());
            user1.setEmail(user.getEmail());
            user1.setPassword(user.getPassword());
            userRepo.save(user1);
        } else {
            throw new RuntimeException("Not found this id" + id);
        }
        return id;
    }

    @Override
    public User login(UserDto userDto) {
        String email = userDto.getEmail();
        String password = userDto.getPassword();
        User user = userRepo.checkEmail(email);

        if (user == null){
            return null;
        } else if (user != null) {
            return user;
        }
        if (!user.getPassword().equals(password)) {
            return null;
        }
        return user;
    }
}
