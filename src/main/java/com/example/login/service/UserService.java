package com.example.login.service;

import com.example.login.dto.UserDto;
import com.example.login.entity.User;

import java.util.List;

public interface UserService {
    List<User> allUser();

    User registr(User user);

    String getId(Long id);

    void deleteUserId(Long id);

//    User login(LoginRequestDTO loginRequest);


    Long update(Long id, User user);

    User login(UserDto userDto);
}
