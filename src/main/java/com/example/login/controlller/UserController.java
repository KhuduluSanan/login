package com.example.login.controlller;

import com.example.login.entity.User;
import com.example.login.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/{all}")
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<User> getAllUser() {
        return userService.allUser();
    }

    @PostMapping("/{registr}")
    public User add(@RequestBody User user) {
        return userService.registr(user);
    }

    @GetMapping("/{id}")
    public String getIdUser(@PathVariable Long id) {
        userService.getId(id);
        return "ID - information : " + id;
    }

    @PutMapping("/{id}")
    public Long updateUser(@PathVariable Long id , @RequestBody User user){
        return userService.update(id,user);
    }

    @DeleteMapping("/{id}")
    public void deleteId(@PathVariable Long id){
         userService.deleteUserId(id);

    }
}
