package com.example.login.controlller;

import com.example.login.dto.UserDto;
import com.example.login.entity.User;
import com.example.login.repository.UserRepo;
import com.example.login.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/login")
public class LoginController {
    private final UserRepo userRepo;
    private final UserService userService;
    @PostMapping
    public User login(@RequestBody UserDto userDto) {
      return userService.login(userDto);
    }


}
